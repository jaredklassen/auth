import React, { Component } from 'react';
import { View } from 'react-native';
import { Header, Button, Spinner, CardSection } from './components/common';
import firebase from 'firebase';
import LoginForm from './components/LoginForm';

class App extends Component {
  state = { loggedIn: null };

  componentWillMount() {
    firebase.initializeApp({
      apiKey: 'AIzaSyDIq-AHwc-EqK2w1CXUV2EU75CYPpusG_A',
      authDomain: 'auth-178e9.firebaseapp.com',
      databaseURL: 'https://auth-178e9.firebaseio.com',
      projectId: 'auth-178e9',
      storageBucket: 'auth-178e9.appspot.com',
      messagingSenderId: '1096205038865'
    });

    firebase.auth().onAuthStateChanged((user) => {
      if (user) {
        this.setState({ loggedIn: true });
      }
      else {
        this.setState({ loggedIn: false });
      }
    });
  }

  renderContent() {
    switch(this.state.loggedIn)
    {
      case true:
      return (<CardSection>
                <Button onPress={() => firebase.auth().signOut()}>Log Out</Button>
              </CardSection>);
      case false:
        return <LoginForm />;
      default:
        return <CardSection><Spinner size="large" /></CardSection>;
    }
  }

  render() {
    return (
      <View>
        <Header headerText="Authentication" />
        {this.renderContent()}
      </View>
    );
  }
}

export default App;
